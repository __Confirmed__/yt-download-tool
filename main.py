import sys
import pytube
import getpass
import os
import fcntl, termios, struct #getwinsize


def download():
    while(1):
        os.system("clear")
        draw_es(w/2-12)
        sys.stdout.write("  Download  ")
        draw_es(w/2-12)
        print('')
        link = raw_input("Video Link: ")
        yt = pytube.YouTube(link)
        print("Available downloads: ")
        print(yt.streams.all())
        print("Enter the itag to download: ")
        num = int(input())
        stream = yt.streams.get_by_itag(num)
        path = raw_input("Enter the path to download(def for default): ")
        if (path == "def"):
            path = "/home/" + getpass.getuser()
        stream.download(path)
        print("Download completed.")
        os.system("sleep 2")
        break

def help():
    draw_es(w/2-8)
    sys.stdout.write("  Help  ")
    draw_es(w/2-8)
    print('')

def draw_es(num):
    for i in range(1, num):
        sys.stdout.write('=')

def getwinsize():
    # from https://stackoverflow.com/questions/566746/how-to-get-linux-console-window-width-in-python
    h, w, hp, wp = struct.unpack('HHHH',
        fcntl.ioctl(0, termios.TIOCGWINSZ,
        struct.pack('HHHH', 0, 0, 0, 0)))
    return w #cols amount

print("Welcome to YT Download Tool. Based on PyTube")
while(1):
    w = getwinsize()
    action = "" #clear the current string value
    os.system("clear")
    print("================================  Menu  ========================================") # 80 - standard terminal size. In future should be equal to window's parameter
    print("D - download video")
    print("H - help")
    print("X - exit")
    action = raw_input("What shall we do now? ").lower()
    if (action == 'd'):
        download()
    elif (action == 'h'):
        help()
    elif (action == 'x'):
        exit()
    else:
        print("Invalid input. Try again")
